from clearml import PipelineController


def step_one(pickle_data_url):
    return 5


def step_two(data_frame, test_size=0.2, random_state=42):
    return 1, 2, 3, 4


def step_three(data):
    return 6


if __name__ == '__main__':
    pipe = PipelineController(
        project='examples',
        name='Pipeline demo',
        version='1.1',
        add_pipeline_tags=False,
    )
    pipe.set_default_execution_queue('default')

    pipe.add_parameter(
        name='url',
        description='url to pickle file',
        default='https://github.com/allegroai/events/raw/master/odsc20-east/generic/iris_dataset.pkl'
    )
    pipe.add_function_step(
        name='step_one',
        function=step_one,
        function_kwargs=dict(pickle_data_url='${pipeline.url}'),
        function_return=['data_frame'],
        cache_executed_step=True,
    )
    pipe.add_function_step(
        name='step_two',
        # parents=['step_one'],  # the pipeline will automatically detect the dependencies based on the kwargs inputs
        function=step_two,
        function_kwargs=dict(data_frame='${step_one.data_frame}'),
        function_return=['processed_data'],
        cache_executed_step=True,
    )
    pipe.add_function_step(
        name='step_three',
        # parents=['step_two'],  # the pipeline will automatically detect the dependencies based on the kwargs inputs
        function=step_three,
        function_kwargs=dict(data='${step_two.processed_data}'),
        function_return=['model'],
        cache_executed_step=True,
    )

    pipe.start()
